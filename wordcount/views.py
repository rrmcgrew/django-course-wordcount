from django.shortcuts import render
import operator
import re


def home(request):
    return render(request, "home.html", {
        "greeting": "My name Borat!"
    })


def count(request):
    fulltext = request.GET["fulltext"]
    stripped = re.sub(r"[^a-zA-Z0-9 ]", "", fulltext).lower()
    words = stripped.split()
    index = {}

    for word in words:
        if word in index:
            # increment the count
            index[word] += 1
        else:
            # add to index
            index[word] = 1

    sorted_index = sorted(
        index.items(),
        key=operator.itemgetter(1),
        reverse=True)

    return render(request, "count.html", {
        "fulltext": fulltext,
        "count":    len(words),
        "index":    sorted_index
    })


def about(request):
    return render(request, "about.html")
